window.onload = function() {
	// Data
	var game, score, keeper, ball, goal, chances, count, contextText, attemptText;
	var scoreCount = 0;
	var penalties = 3;

	var ballBottom;

	var countDown = 3;
	var countDownTimer ;

	// Controls
	var keydown = {};
	// States
	var moveBall = false;
	var keeperReady = true;
	var goalScored = false;
	var goalSaved = false;
	var reset = false;
	// Frame Rate
	var fps = 30;

	// Game setup
	game = document.getElementById("foosball");
	game.focus();
	game.width = 300;
	game.height = 250;
	pitch = game.getContext("2d");


	var phantomFoosball = {
		init: function() {
			console.log('Fire up the foosball');

			// Game elements
			// Keeper  Element
			keeper = { 
				color: '#666',
				x: 130, 
				y: 180, 
				w: 40,
				h: 20,
				draw: function() {
					pitch.fillStyle = this.color;
					pitch.fillRect(this.x, this.y, this.w, this.h);					
				},
				clear: function() {
					pitch.clearRect(this.x, this.y, this.w, this.h);
				}
			};

			// Goal Element
			goal = {
				color: '#000',
				x: 50, 
				y: 210, 
				w: 200, 
				h: 40,
				draw: function() {
					pitch.strokeStyle = this.color;	
					pitch.strokeRect(this.x, this.y, this.w, this.h);
				},
				clear: function() {
					pitch.clearRect(this.x, this.y, this.w, this.h);
				}
			};

			// Calculate shot
			var getRandomX = function(min, max) {
				return Math.floor(Math.random() * (max - min)) + min;
			}

			// Ball Element
			ball = {
				color: "red",
				x: 150,
				y: 20,
				shotX: '',
				velocity: '',
				radius: 10,
				draw: function() {
					pitch.beginPath();
					pitch.fillStyle= this.color;
					pitch.arc(this.x, this.y, this.radius, 0, 2*Math.PI);
					pitch.fill();
				},
				clear: function() {
					pitch.beginPath();
					pitch.clearRect(this.x - this.radius - 1, this.y - this.radius - 1, this.radius * 2 + 2, this.radius * 2 + 2);
					pitch.closePath();
				},
				shotLocation: function() {
					// Use width of goal to calculate min and max range (minus a little avoid posts)
					return getRandomX(75, 225);
				}
			};
			ballBottom = ball.y + ball.radius + 1;

			// Set Scorecard & Fonts
			score = {
				color: "white",
				x: 225,
				y: 10,
				draw: function() {
					pitch.fillStyle = this.color;
					pitch.font = "16px Helvetica";
					pitch.textBaseline = "top";
					pitch.fillText("Score : " + scoreCount, this.x, this.y);					
				},
				clear: function() {
					pitch.clearRect(this.x, this.y, 100, 20);
				}
			}

			// Set chances`
			chances = {
				color: "white",
				x: 0,
				y: 15,
				radius: 5,
				draw: function() {
					pitch.beginPath();
					pitch.fillStyle= this.color;
					pitch.arc(this.x, this.y, this.radius, 0, 2*Math.PI);
					pitch.fill();					
				},
				clear: function() {
					pitch.beginPath();
					pitch.clearRect(0, 0, 100, 100);
					pitch.closePath();					
				}
			}

			// Shot Countdown	
			count = {
				color: "white",
				x: 262	,
				y: 195,
				draw: function() {
					pitch.fillStyle = this.color;
					pitch.font = "46px Helvetica";
					pitch.textBaseline = "top";
					pitch.fillText(countDown, this.x, this.y);						
				},
				clear: function() {
					pitch.clearRect(this.x, this.y, 300, 250);
				}				
			}

			contextText = {
				color: 'white',
				x: 65,
				y: 110,
				draw: function() {
					pitch.fillStyle = this.color;
					pitch.font = "52px Helvetica";
					pitch.textBaseline = "middle";
					pitch.fillText(attemptText, this.x, this.y);
				},
				clear: function() {
					pitch.clearRect(this.x,  50, 200, 100);
				}
			}

			// Draw Elements & Reiterate Drawing
			setTimeout(function() {
				requestAnimationFrame(phantomFoosball.draw);
				requestAnimationFrame(phantomFoosball.update);
			}, 1000 / fps);

			// Draw point element
			score.draw();
			// Draw countdown till element
			count.draw();

			// Draw chances remaining
			for(var i = 0; i < penalties; i++) {
				chances.x = chances.x + 15;
				chances.draw();
			}			

			// Start Countdown
			this.shootBall();

			// Keyboard Event Listeners & Key Identifiers
			function keyName(event) {
				return event.keyIdentifier.toLowerCase();
			}			

			// Listen for left/right keypress
			game.addEventListener("keydown", function(event) {
				keydown[keyName(event)] = true;
			}, true);
			game.addEventListener("keyup", function(event) {
				keydown[keyName(event)] = false;	
			}, true);
		},
		draw: function() {

			// redraw keeper
			keeper.draw();

			// redraw ball
			ball.draw();

			// redraw goal
			goal.draw();
		},
		update: function() {
			
			// Clear elements
			keeper.clear();
			ball.clear();
			goal.clear();

			// Move keeper && stop keeper moving outside his goal area
			if(keeperReady){
				if(keydown.left && keeper.x >= 50) {
					keeper.x -= 10;
				}

				if(keydown.right && keeper.x <= (250 - keeper.w)) {
					keeper.x += 10;
				}
			}

			if(moveBall) {
				ball.x = ball.x + ball.velocity;
				ball.y += 6;
			}

			// Check for Collisions
			var collides = function(theBall, b, context) {
				// console.log('check collision');
				switch(context) {
					case "score":
						// console.log('check for goal');
						return theBall.x > b.x && theBall.x < b.x + b.w && theBall.y > b.y && theBall.y < b.y + b.h;
					break;
					case "save":
						// console.log('check for save');
						return theBall.x - 5 > b.x && theBall.x < b.x + b.w + 5 && theBall.y > b.y && theBall.y < b.y + b.h;
						// return true if theBalls X value is greater than keepers X and theBalls X value is less than keepers X value plus the keepers width and if theBalls Y value is greater than keepers Y value and theBalls Y value is less than keepers y value plus height.
					break;
				}
			}

			// Are the ball & keeper touching
			if(ball.y > keeper.y && collides(ball, keeper, 'save')) {
				// console.log('goalSaved');
				goalSaved = true;
				phantomFoosball.save();
			}

			// Is the ball in the goal
			if(ball.y > goal.y && collides(ball, goal, 'score')) {
				// console.log('goalScored');
				goalScored = true;
				phantomFoosball.goal();
			}

			// Debug reset game if ball goes off pitch
			// if(ball.y > 300 && !reset) {
			// 	phantomFoosball.reset();
			// }

			requestAnimationFrame(phantomFoosball.update);
			requestAnimationFrame(phantomFoosball.draw);

		},
		shootBall: function() {
			// Stop Timer && Allow shot
			var stopCountDown = function() {
				keeperReady = true;
				clearInterval(countDownTimer);
				ball.shotX = ball.shotLocation();
				ball.velocity = (ball.shotX - ball.x) / fps;

				countDown = '';
				count.draw();
			}

			// Timer to allow player to get ready
			countDownTimer = setInterval( function() {
				if(countDown !== 1) {
					countDown --;
				} else {
					stopCountDown();
					moveBall = true;
				}
				count.clear();
				count.draw();
			}, 1000);

		},
		goal: function() {
			phantomFoosball.reset();
			// If ball goes in fire function to declare goal
			if(goalScored) {
				attemptText = 'GOAL!';
				contextText.draw();

				scoreCount --;
				score.clear();
				score.draw();
				goalScored = false;
			}

		},
		save:  function() {
			phantomFoosball.reset();
			// If ball collides with keeper to declare miss/save
			if(goalSaved) {
				attemptText = 'SAVE!';
				contextText.draw();

				scoreCount ++;
				score.clear();
				score.draw();
				goalSaved = false;
			}
		},
		reset: function() {
			// Reset Ball, Update Chances & Score
			reset = true;
			// goalSaved = false;
			// goalScored = false;

			// Replace Ball
			moveBall = false;
			ball.y = 20;
			ball.x = 150;			

			setTimeout( function() {
				// console.log('reset the game');
				contextText.clear();
				countDown = 3;
				count.draw();

				// Remove Chance
				penalties --;
				chances.clear();
				chances.x = 0;

				// Draw chances remaining
				for(var i = 0; i < penalties; i++) {
					chances.x = chances.x + 15;
					chances.draw();
				}

				// If all chances are up finish game
				if(penalties === 0) {
					phantomFoosball.endGame();
				} else {
					var stopCountDown = function() {
						keeperReady = true;
						clearInterval(countDownTimer);
						ball.shotX = ball.shotLocation();
						ball.velocity = (ball.shotX - ball.x) / fps;
						countDown = '';
						count.draw();
					}			

					countDownTimer = setInterval( function() {
						if(countDown !== 1) {
							countDown --;
						} else {
							stopCountDown();
							moveBall = true;
							reset = false;
						}
						count.clear();
						count.draw();
					}, 1000);				
				}
			}, 2000);
			
		},
		endGame: function() {
			// When all 3 Chances are up Kill game.
			attemptText = 'Gameover!';
			contextText.x = 20;
			contextText.draw(); 

			count.clear();

			keeper.clear();	
			keeperReady = false;
			keeper.x = 130;			

		}
	};
	phantomFoosball.init();
};
